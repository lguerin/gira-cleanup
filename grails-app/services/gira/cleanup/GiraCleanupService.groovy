package gira.cleanup

import gira.Comment
import grails.gorm.transactions.Transactional

@Transactional
class GiraCleanupService {

    List<Comment> getLastComments(int max) {
        return Comment.executeQuery("from Comment order by dateCreated desc", [max: max])
    }

    def cleanupLastComments(int batchSize) {
        def comments = this.getLastComments(batchSize)
        List<Integer> ids = comments*.id
        Comment.executeUpdate("DELETE from Comment where id in :ids", [ids: ids])
        return true
    }
}
