package gira.cleanup

import java.util.concurrent.atomic.AtomicInteger

class PurgeCommentsJob {

    /**
     * Thead-safe session counter
     */
    private static AtomicInteger count = new AtomicInteger(0)

    GiraCleanupService giraCleanupService

    static triggers = {
      simple repeatInterval: 3000l
    }

    def execute() {
        int batchSize = 1000
        giraCleanupService.cleanupLastComments(batchSize)
        count.set(count + batchSize)
        if (count % 10000 == 0) {
            println "Number of deleted comments: ${count}"
        }
    }
}
